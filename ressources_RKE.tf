resource "oci_core_instance" "generated_oci_core_instance" {
	availability_config {
		recovery_action = "RESTORE_INSTANCE"
	}
	# Account Region
	availability_domain = "igVL:EU-MARSEILLE-1-AD-1"
	compartment_id = "ocid1.tenancy.oc1..aaaaaaaak5a6k3z4cbf7ud4ur5f3gixbxhqskm5t7vrsclzawnre6unuqcia"

	# Create Subnet if doesn't exist or attach one
	create_vnic_details {
		assign_private_dns_record = "true"
		assign_public_ip = "true"
		subnet_id = "ocid1.subnet.oc1.eu-marseille-1.aaaaaaaadqy33sxtbiwwsc4owk7m2lc2eice2hj4edirgc65ck24bpzdhhuq"
	}
	# Instance basic details
	display_name = "RKE1"
	shape = "VM.Standard.A1.Flex"
	shape_config {
		memory_in_gbs = "12"
		ocpus = "2"
	}
	source_details {
		source_id = "ocid1.image.oc1.eu-marseille-1.aaaaaaaaoygutjey3po3dwskuov5wr2t7rz7oiwscnwentxlt6fk4jknpylq" # Image ID from the region
		source_type = "image"
	}
}
