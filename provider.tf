provider "oci" {
    auth = var.auth
    region = var.region
}
